CREATE TABLE base_index (name text,  id uuid, PRIMARY KEY(name, id));
CREATE TABLE rule (id uuid PRIMARY KEY, name text, server_link_id uuid, client_link_id uuid, admin_state boolean);
CREATE TABLE server_link (
    id uuid,
    rule_id uuid,
    name text,
    description text,
    host text,
    port text,
    system_id text,
    password text,
    ton int,
    npi int,
    PRIMARY KEY(rule_id, id));