package com.a1s.imp.dao;

import com.a1s.imp.domain.BaseIndex;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.convert.CassandraEntityConverter;
import org.springframework.data.cassandra.repository.BaseCassandraRepository;
import org.springframework.data.cassandra.template.CassandraTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author Alexey Belov
 */
@Repository
public class BaseIndexDaoCassandra extends BaseCassandraRepository<BaseIndex, String> implements BaseIndexDao {

    public static final String CF_NAME = "base_index";

    @Override
    public Iterable<BaseIndex> list(String name) {
        final Select select = QueryBuilder.select().all().from(CF_NAME);
        select.where(QueryBuilder.eq("name", name));
        Iterable<BaseIndex> result = getListByQuery(select);
        return result;
    }

    @Override
    public void add(BaseIndex baseIndex) {
        save(baseIndex);
    }

    @Override
    public void remove(BaseIndex baseIndex) {
        delete(baseIndex);
    }

    @Override
    protected Class<?> getEntityClass() {
        return BaseIndex.class;
    }

    @Autowired
    @Override
    public void setTemplate(CassandraTemplate template) {
        super.setTemplate(template);
    }

    @Autowired
    @Override
    public void setConverter(CassandraEntityConverter converter) {
        super.setConverter(converter);
    }

    @Override
    protected void queryReadOptions(Statement query) {
        query.setConsistencyLevel(ConsistencyLevel.QUORUM);
    }


}
