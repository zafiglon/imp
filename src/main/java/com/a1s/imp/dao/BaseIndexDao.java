package com.a1s.imp.dao;

import com.a1s.imp.domain.BaseIndex;

/**
 * @author Alexey Belov
 */
public interface BaseIndexDao{

    public Iterable<BaseIndex> list(final String name);
    public void add(BaseIndex baseIndex);
    public void remove(BaseIndex baseIndex);

}
