package com.a1s.imp.dao;

import com.a1s.imp.domain.BaseIndex;
import com.a1s.imp.domain.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.repository.BaseCassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * @author Alexey Belov
 */
@Repository
public class RuleDaoCassandra extends BaseCassandraRepository<Rule, UUID> implements RuleDao {

    public static final String CF_NAME = "rule";


    @Autowired
    private BaseIndexDao baseIndexDao;

    @Override
    public List<Rule> list() {
//        final Iterable<BaseIndex> list = baseIndexDao.list(Rule.class.getName());
//        final List<UUID> collect = list.stream().map(bi -> bi.getKey().getId()).collect(Collectors.toList());
//
//        return (List<Rule>) findAll(collect);
        return null;
    }

    @Override
    public void remove(Rule rule) {
        delete(rule);
        final BaseIndex baseIndex = new BaseIndex(rule.getId(), rule.getName());
        baseIndexDao.remove(baseIndex);
    }

    @Override
    public void create(Rule rule) {
        save(rule);
        final BaseIndex baseIndex = new BaseIndex(rule.getId(), Rule.class.getName());
        baseIndexDao.add(baseIndex);
    }

    @Override
    public void update(Rule rule) {
        update(rule);
    }

    @Override
    public Rule get(UUID id) {
        return findOne(id);
    }

    @Override
    protected Class<?> getEntityClass() {
        return Rule.class;
    }
}
