package com.a1s.imp.dao;

import com.a1s.imp.dao.keys.ServerLinkKey;
import com.a1s.imp.domain.ServerLink;
import org.springframework.data.cassandra.repository.CassandraRepository;

/**
 * @author Alexey Belov
 */
public interface ServerLinkDao extends CassandraRepository<ServerLink, ServerLinkKey> {


}
