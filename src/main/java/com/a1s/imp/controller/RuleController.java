package com.a1s.imp.controller;

import com.a1s.imp.domain.Rule;
import com.a1s.imp.services.RuleService;
import com.datastax.driver.core.utils.UUIDs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
public class RuleController {

	@Autowired
    RuleService ruleService;

	@RequestMapping("/createRequest")
	public ModelAndView registerUser(@ModelAttribute Rule user) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", UUIDs.timeBased());
		return new ModelAndView("ruleCreate", "map", map);
	}

	@RequestMapping("/create")
	public String create(@ModelAttribute Rule rule) {
		if (rule != null)
			ruleService.create(rule);
		return "redirect:list";
	}

	@RequestMapping("/list")
	public ModelAndView getUserLIst() {
		List<Rule> ruleList = ruleService.list();
		return new ModelAndView("ruleList", "ruleList", ruleList);
	}

	@RequestMapping("/edit")
	public ModelAndView update(@ModelAttribute Rule rule) {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("user", rule);

		return new ModelAndView("ruleEdit", "map", map);

	}

	@RequestMapping("/update")
	public String updateUser(@ModelAttribute Rule rule) {
		ruleService.update(rule);
		return "redirect:list";

	}

	@RequestMapping("/remove")
	public String deleteUser(@RequestParam UUID id) {
		System.out.println("id = " + id);
        final Rule rule = ruleService.get(id);
        ruleService.remove(rule);
		return "redirect:list";
	}
}