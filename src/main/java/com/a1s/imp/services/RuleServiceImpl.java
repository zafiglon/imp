package com.a1s.imp.services;

import com.a1s.imp.dao.RuleDao;
import com.a1s.imp.domain.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * @author Alexey Belov
 */
@Service
public class RuleServiceImpl implements RuleService {

    @Autowired
    private RuleDao ruleDao;

    @Override
    public List<Rule> list() {
        return ruleDao.list();
    }

    @Override
    public void remove(Rule rule) {
        ruleDao.remove(rule);
    }

    @Override
    public void create(Rule rule) {
        ruleDao.create(rule);
    }

    @Override
    public void update(Rule rule) {
        ruleDao.update(rule);
    }

    @Override
    public Rule get(UUID id) {
        return ruleDao.get(id);
    }
}
