package com.a1s.imp.services;

import com.a1s.imp.domain.Rule;

import java.util.List;
import java.util.UUID;

/**
 * @author Alexey Belov
 */
public interface RuleService {

    public List<Rule> list();
    public void remove(final Rule rule);
    public void create(final Rule rule);
    public void update(final Rule rule);
    public Rule get(final UUID id);

}
