package com.a1s.imp.domain;

import com.a1s.imp.dao.ServerLinkDaoCassandra;
import com.a1s.imp.dao.keys.ServerLinkKey;
import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.Table;

/**
 * @author Alexey Belov
 */
@Table(value = ServerLinkDaoCassandra.CF_NAME)
public class ServerLink {

    @Id
    private ServerLinkKey key;
    @Column(value = "name")
    private String name;
    @Column(value = "description")
    private String description;
    @Column(value = "host")
    private String host;
    @Column(value = "port")
    private Integer port;
    @Column(value = "system_id")
    private String systemId;
    @Column(value = "password")
    private String password;
    @Column(value = "ton")
    private Integer ton;
    @Column(value = "npi")
    private Integer npi;

    public ServerLinkKey getKey() {
        return key;
    }

    public void setKey(ServerLinkKey key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getTon() {
        return ton;
    }

    public void setTon(Integer ton) {
        this.ton = ton;
    }

    public Integer getNpi() {
        return npi;
    }

    public void setNpi(Integer npi) {
        this.npi = npi;
    }
}
