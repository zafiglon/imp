package com.a1s.imp.domain;

import com.a1s.imp.dao.RuleDaoCassandra;
import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.Table;

import java.util.UUID;

/**
 * @author Alexey Belov
 */
@Table(value = RuleDaoCassandra.CF_NAME)
public class Rule {

    @Id
    private UUID id;
    @Column(value = "name")
    private String name;
    @Column(value = "server_link_id")
    private UUID serverLinkId;
    @Column(value = "client_link_id")
    private UUID clientLinkId;
    @Column(value = "admin_state")
    private Boolean adminState;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getServerLinkId() {
        return serverLinkId;
    }

    public void setServerLinkId(UUID serverLinkId) {
        this.serverLinkId = serverLinkId;
    }

    public UUID getClientLinkId() {
        return clientLinkId;
    }

    public void setClientLinkId(UUID clientLinkId) {
        this.clientLinkId = clientLinkId;
    }

    public Boolean getAdminState() {
        return adminState;
    }

    public void setAdminState(Boolean adminState) {
        this.adminState = adminState;
    }
}
