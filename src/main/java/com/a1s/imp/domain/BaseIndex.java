package com.a1s.imp.domain;


import com.a1s.imp.dao.BaseIndexDaoCassandra;
import com.a1s.imp.dao.keys.BaseIndexKey;
import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.mapping.Table;

import java.util.UUID;

/**
 * @author Alexey Belov
 */
@Table(value = BaseIndexDaoCassandra.CF_NAME)
public class BaseIndex {

    @Id
    private BaseIndexKey key;

    public BaseIndex(final UUID id, final String name) {
        BaseIndexKey key = new BaseIndexKey();
        key.setId(id);
        key.setName(name);
        this.setKey(key);
    }

    public BaseIndex() {
    }

    public BaseIndexKey getKey() {
        return key;
    }

    public void setKey(BaseIndexKey key) {
        this.key = key;
    }

}
