<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>A1S IMP</title>
<style>
body {
	font-size: 20px;
	color: teal;
	font-family: Calibri;
}

td {
	font-size: 15px;
	color: black;
	width: 100px;
	height: 22px;
	text-align: center;
}
.heading {
	font-size: 18px;
	color: white;
	font: bold;
	background-color: orange;
	border: thick;
}
</style>
</head>
<body>
	<center>
		<br /> <br /> <br /> <b>Rule List </b><br /> <br />
			
		

		<table border="1">
			<tr>
				<td class="heading">ID</td>
				<td class="heading">Name</td>
				<td class="heading">ServerLinkId</td>
				<td class="heading">ClientLinkId</td>
				<td class="heading">AdminState</td>
				<td class="heading">Edit</td>
				<td class="heading">Delete</td>
			</tr>
			<c:forEach var="rule" items="${ruleList}">
				<tr>
					<td>${rule.id}</td>
					<td>${rule.name}</td>
					<td>${rule.serverLinkId}</td>
					<td>${rule.clientLinkId}</td>
					<td>${rule.adminState}</td>
					<td><a href="edit?id=${rule.id}">Edit</a></td>
					<td><a href="remove?id=${rule.id}">Delete</a></td>
				</tr>
			</c:forEach>
			<tr><td colspan="7"><a href="createRequest">Add New Rule</a></td></tr>
		</table>

	</center>
</body>
</html>