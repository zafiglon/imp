<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Being Java Guys | Spring DI Hello World</title>
    <style>
        body {
            font-size: 20px;
            color: teal;
            font-family: Calibri;
        }

        td {
            font-size: 15px;
            color: black;
            width: 100px;
            height: 22px;
            text-align: left;
        }

        .heading {
            font-size: 18px;
            color: white;
            font: bold;
            background-color: orange;
            border: thick;
        }
    </style>
</head>
<body>
<br/> <br/> <br/> <b> Create Rule </b> <br/>
<br/>

<div>
    <form:form method="post" action="create" modelAttribute="rule">
        <table>
            <tr>
                <td>Id : </td>
                <td><form:hidden path="id" value="${map.id}"/></td>
            </tr>
            <tr>
                <td>Server Link Id :</td>
                <td><form:input path="serverLinkId"/></td>
            </tr>
            <tr>
                <td>Client Link Id :</td>
                <td><form:input path="clientLinkId"/></td>
            </tr>
            <tr>
                <td>Name :</td>
                <td><form:input path="name"/></td>

            </tr>
            <tr>
                <td>Admin State :</td>
                <td><form:hidden path="adminState" value="false" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" value="Save"/></td>
            </tr>
            <tr>

                <td colspan="2"><a href="list">Click Here to See User List</a></td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>