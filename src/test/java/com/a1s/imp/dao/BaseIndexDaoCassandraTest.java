package com.a1s.imp.dao;

import com.a1s.imp.dao.keys.BaseIndexKey;
import com.a1s.imp.domain.BaseIndex;
import com.datastax.driver.core.utils.UUIDs;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Alexey Belov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/applicationContextTest.xml"})
public class BaseIndexDaoCassandraTest {
    @Autowired
    BaseIndexDao baseIndexDaoCassandra;

    @Test
    public void test() {
        BaseIndex i = new BaseIndex();
        BaseIndexKey key = new BaseIndexKey();
        key.setId(UUIDs.timeBased());
        key.setName("Rule");
        i.setKey(key);
        baseIndexDaoCassandra.add(i);
        final Iterable<BaseIndex> rule = baseIndexDaoCassandra.list("Rule");
        rule.forEach(r -> {
            System.out.println(r.getKey().getId());
        });
    }




}
