package com.a1s.imp.dao;

import com.a1s.imp.domain.Rule;
import com.datastax.driver.core.utils.UUIDs;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

/**
 * @author Alexey Belov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/applicationContextTest.xml"})
public class RuleDaoCassandraTest {
    @Autowired
    RuleDao ruleDao;

    @Test
    public void test() {
        Rule rule = new Rule();
        final UUID ruleId = UUIDs.timeBased();
        rule.setId(ruleId);
        rule.setName("1");
        rule.setClientLinkId(UUIDs.timeBased());
        rule.setServerLinkId(UUIDs.timeBased());
        rule.setAdminState(true);
        ruleDao.create(rule);
        final Rule rule1 = ruleDao.get(ruleId);
        Assert.assertEquals(rule.getName(), rule1.getName());
        rule1.setName("2");
        ruleDao.update(rule1);
        final Rule rule2 = ruleDao.get(ruleId);
        Assert.assertEquals("2", rule2.getName());
        ruleDao.remove(rule2);



    }




}
